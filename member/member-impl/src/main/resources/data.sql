

INSERT INTO Member (member_id, greetings) values
    (x'c459a5dc56d44bd6b628e3ca6b37c1db', 'Tom Mishch'),
    (x'fb212080a4ed4224a2e10ee0f2bf72c1', 'Michael Jackson'),
    (x'e1d5575a10d948fe8b8524c54b3d5fb3', 'Bobby Helms'),
    (x'3a387b6984c54b81a0b13c5bc0361201', 'Jose Feliciano'),
    (x'455c2197db12411a8345d245940c4361', 'Brenda Lee')
;
