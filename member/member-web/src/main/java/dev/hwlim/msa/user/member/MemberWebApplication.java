package dev.hwlim.msa.user.member;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"dev.hwlim.msa.user.**"})
public class MemberWebApplication {

  public static void main(String[] args) {
    SpringApplication.run(MemberWebApplication.class, args);
  }

}
