drop table if exists Member CASCADE;

create table Member
(
    member_id binary  not null,
    greetings       varchar(255),
    primary key (member_id)
);
