package dev.hwlim.msa.user.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"dev.hwlim.msa.user.**"})
public class UserServerApplication {

  public static void main(String[] args) {
    SpringApplication.run(UserServerApplication.class, args);
  }

}
